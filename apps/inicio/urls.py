from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from .views import index

urlpatterns = patterns('',
            url(r'^$','apps.inicio.views.index')
)
