from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from .forms import LoginForm
from django.contrib.auth import authenticate, login, logout
from apps.inicio.views import index



def login_page(request):
    message = None
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(username=username,password=password)
            if user is not None:
                if user.is_active:
                    login(request,user)
                    # return render_to_response('inicio/index.html',{'message':message},context_instance=RequestContext(request))
                    return redirect('index')
                else:
                    message = "Tu usuario esta inactivo"
            else:
                message = "error"
    else:
        form = LoginForm()
    return render_to_response('inicio/login.html',{'message':message,'form':form},context_instance=RequestContext(request))

def homepage(request):
    return index(request)

def logout_view(request):
    logout(request)
    return redirect('login')




            # if user is not None:
            #     if user.is_active:
            #         login(request,user)
            #         if user.is_authenticated:
            #             message = user.username
            #         return render_to_response('inicio/index.html',{'message':message,'form':form},context_instance=RequestContext(request))
            #     else:
            #         message = "Tu usuario esta inactivo"
            # else:
            #     message = "error"
