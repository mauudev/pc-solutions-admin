from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pcsadmin.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', include('apps.inicio.urls')),
    url(r'^$','pcsadmin.views.homepage',name="index"),
    url(r'^login/$','pcsadmin.views.login_page',name='login'),
    url(r'^logout/$','pcsadmin.views.logout_view',name='logout'),
)
